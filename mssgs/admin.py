from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import Message, CategoriesSet, Category

class MessageAdmin(admin.ModelAdmin):
    fields = ['message_text', 'group', 'category']
    list_display = ['message_text', 'date_create', 'date_updated', 'group', 'category']
    search_fields = ['message_text']

class CategoriesSetAdmin(admin.ModelAdmin):
    pass

admin.site.register(Message, MessageAdmin)
admin.site.register(CategoriesSet, CategoriesSetAdmin)

admin.site.register(
    Category,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
    ),
    list_display_links=(
        'indented_title',
    ),
)
