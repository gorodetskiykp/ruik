from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404, render, redirect
from django.views import generic
from django.contrib import messages
from django.views.generic.edit import FormView, CreateView
# from django.core.mail import send_mail
from django_tables2 import RequestConfig
from formtools.wizard.views import SessionWizardView
from django.core.files.storage import FileSystemStorage
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView

from django.http import JsonResponse

import os
import logging

logger = logging.getLogger(__name__)

from .tables import MessageTable
from .models import Message, Category
from grps.models import Group
from .forms import MessageForm

from django.forms.widgets import TextInput

def upload_location(instance, filename):
    return "{}/{}".format(datetime.now().strftime("%Y-%m"), filename)


# def index(request):
#     table = MessageTable(Message.objects.all())
#     RequestConfig(request, paginate={'per_page': 50}).configure(table)
#     return render(request, 'mssgs/messages_table.html', {'table': table})

class MessageList(ListView):
    model = Message
    context_object_name = 'message_list'

@csrf_exempt
def webcam_upload(request):
    if request.POST:
        f = open(settings.MEDIA_ROOT + '/webcamimages/someimage.jpg', 'wb')
        f.write(request.raw_post_data)
        f.close()
        return HttpResponse('http://localhost:8080/site_media/webcamimages/someimage.jpg')
    else:
        logger.info('iSomething went wrong!')
        logger.warning('wSomething went wrong!')
        logger.debug('dSomething went wrong!')
        logger.error('eSomething went wrong! {}'.format(request.POST))
        return HttpResponse('no data')


def user_info(request):
    user_info = request.META['REMOTE_USER']
    #user_info = request.environ.get('REMOTE_USER')
    return render(request, 'mssgs/user_info.html', {'user_info': user_info})


class DetailView(generic.DetailView):
    model = Message


class MessageView(CreateView):
    model = Message
    form_class = MessageForm
    template_name = 'mssgs/message_form.html'
    success_url = 'messages'

    def get_context_data(self, **kwargs):
        group_id = self.kwargs['group']
        context = super(MessageView, self).get_context_data(**kwargs)
        context['group'] = Group.objects.get(pk=group_id)
        return context

    def get_initial(self):
        initial = super(MessageView, self).get_initial()
        group_id = self.kwargs['group']
        group = Group.objects.get(pk=group_id)
        initial['group'] = group.id
        return initial

    # def get_form(self, form_class):
    #     group_id = self.kwargs['group']
    #     form = super(MessageView, self).get_form(form_class)
    #
    #     form.fields['category'] = Category.objects.filter(category_set=Group.objects.get(id=group_id).category_set)
    #     return form

# class MessageWizard(SessionWizardView):
#     form_list = [MessageFormGroup, MessageFormCategory, MessageFormText]
#
#     def get_template_names(self):
#         return ['mssgs/message_form_step.html']
#     # file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'temp'))
#
#     def done(self, form_list, **kwargs):
#         pass
#         return HttpResponseRedirect('')

def change_message_status(request):
    # username = request.GET.get('username', None)
    status = 'В работе'
    data = {
        'is_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)
