from django import forms
from django.forms import ModelForm, HiddenInput, TextInput
from multiupload.fields import MultiImageField
from django.forms.widgets import RadioSelect, FileInput, Select, CheckboxInput

from .models import Message, MessageImageAttachment, Category
from grps.models import Group
from mptt.forms import TreeNodeChoiceField

class MessageForm(ModelForm):

    def __init__(self, *args, **kwargs):
        group_id = kwargs['initial']['group']
        super(MessageForm, self).__init__(*args, **kwargs)
        self.fields['images'].required = False
        category = TreeNodeChoiceField(Category.objects.filter(category_set=Group.objects.get(id=group_id).category_set), level_indicator=u'→')
        self.fields['category'] = category
        self.fields['category'].empty_label = 'Категория...'
        self.fields['category'].label = 'Категория'

    class Meta:
        model = Message
        fields = ['group', 'category', 'message_text']
        widgets = {
            'group': HiddenInput(),
        }

    images = MultiImageField(min_num=1, max_num=3,
                             max_file_size=1024 * 1024 * 10)


    def save(self, commit=True):
        # if Category.objects.filter(parent=self.cleaned_data['category']):
        #     raise Exception('parent')
        # else:
        #     raise Exception('child')
        instance = super(MessageForm, self).save(commit)
        for each in self.cleaned_data['images']:
            MessageImageAttachment.objects.create(
                image_file=each, message=instance)
        return instance

#
# class MessageFormGroup(ModelForm):
#     class Meta:
#         model = Message
#         fields = ['group']
#         widgets = {
#             'group': RadioSelect,
#         }


# class MessageFormCategory(ModelForm):
#     class Meta:
#         model = Message
#         fields = ['category']


# class MessageFormText(ModelForm):
#     class Meta:
#         model = Message
#         fields = ['message_text']

    # images = MultiImageField(min_num=1, max_num=3, max_file_size=1024*1024*10)
    #
    # def save(self, commit=True):
    #     instance = super(MessageForm, self).save(commit)
    #     for each in self.cleaned_data['images']:
    #         MessageImageAttachment.objects.create(image_file=each, message=instance)
    #     return instance
