from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey

def upload_location(instance, filename):
    return "{}/{}".format(datetime.now().strftime("%Y-%m"), filename)

class CategoriesSet(models.Model):
    categories_set_name = models.CharField(max_length=250, verbose_name='Набор категорий')

    def __str__(self):
        return self.categories_set_name

    class Meta:
        ordering = ['categories_set_name']
        verbose_name = 'Набор категорий'
        verbose_name_plural = 'Наборы категорий'

class Category(MPTTModel):
    category        = models.CharField(max_length=250, verbose_name='Категория')
    parent          = TreeForeignKey('self',  null=True, blank=True, verbose_name='Родительская категория', related_name='children', db_index=True)
    category_set    = models.ForeignKey(CategoriesSet,  null=True, blank=True, verbose_name='Набор категорий', related_name='categories')

    def __str__(self):
        return self.category

    class Meta:
        ordering = ['category']
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    class MPPTMeta:
        order_insertion_by = ['category']

class Message(models.Model):
    group           = models.ForeignKey('grps.Group', verbose_name='Группа', default='', blank=False, related_name='messages')
    category        = models.ForeignKey(Category, verbose_name='Категория', blank=False, related_name='messages')
    message_text    = models.TextField('Замечание', blank=False, null=False)
    date_create     = models.DateTimeField('Дата создания', auto_now_add=True)
    date_updated    = models.DateTimeField('Дата редактирования', auto_now=True)
    photo           = models.ImageField(upload_to=upload_location, null=True, blank=True)
    author          = models.CharField('Пользователь', max_length=250, null=False, blank=False)

    def __str__(self):
        return '{}'.format(self.message_text)

    class Meta:
        ordering = ['-date_create']
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class MessageImageAttachment(models.Model):
    message     = models.ForeignKey(Message, verbose_name='Замечание', related_name='images')
    image_file  = models.ImageField('ImageAttachment', upload_to=upload_location, null=True, blank=True)


class StatusLog(models.Model):
    message     = models.ForeignKey(Message, verbose_name='Сообщение', blank=False, related_name='status_logs')
    date_create = models.DateTimeField('Дата создания', auto_now_add=True)
    status = models.CharField('Статус задачи', max_length=100, null=False, blank=False, unique=True)
    initiator = models.ForeignKey(User, verbose_name='Пользователь', related_name='status_logs_initiators')
