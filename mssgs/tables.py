import django_tables2 as tables
from django_tables2.utils import A
from .models import Message

class MessageTable(tables.Table):
    message_text = tables.LinkColumn('mssgs:detail', text=lambda record: record.message_text, args=[A('pk')])
    group = tables.LinkColumn('grps:detail', text=lambda record: record.group, args=[A('group.pk')])
    class Meta:
        model = Message
        attrs = {'class': 'table table-bordered'}
        fields = ['id', 'message_text', 'group', 'category', 'date_create']
