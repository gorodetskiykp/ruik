from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from . import views
# from .views import MessageWizard

app_name = 'mssgs'
urlpatterns = [
    url(r'^message$', views.MessageView.as_view(), name='message'),
    # url(r'^messages$', views.IndexView.as_view(), name='messages'),
    url(r'^messages$', views.MessageList.as_view(), name='messages'),
    url(r'^user_info$', views.user_info, name='user_info'),
    url(r'^msg/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # url(r'^new_message$', MessageWizard.as_view(MessageWizard.form_list),
    # name='new_message'),
    url(r'^(?P<group>[0-9]+)/new_message$',
        views.MessageView.as_view(),
        name='new_message'),
    url(r'^webcam_upload$', views.webcam_upload, name='webcam_upload'),
]
