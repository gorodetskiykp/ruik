# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-10 15:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mssgs', '0015_auto_20161111_0100'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoriesset',
            name='group',
        ),
    ]
