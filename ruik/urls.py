from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^mssgs/', include('mssgs.urls')),
    url(r'^groups/', include('grps.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^.*', include('grps.urls')),
]
