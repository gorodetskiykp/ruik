from ruik.settings.common import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTHENTICATION_BACKENDS = [
    # 'django_remote_auth_ldap.backend.RemoteUserLDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
]

STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, "static"),
    r'd:\projects\ruik\static',
)
