from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Employee(models.Model):
    user = models.OneToOneField(User)
    phone = models.CharField(max_length=30)
    
    #@receiver(post_save, sender=User)
    #def create_profile(sender, instance, created, **kwargs):
    #    if created:
    #        profile, new = Employee.objects.get_or_create(user=instance)
