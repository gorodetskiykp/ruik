from django.contrib import admin

from .models import Profile, WorkGroup, Membership

class MembershipInline(admin.TabularInline):
    model = Membership
    extra = 1

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    inlines = (MembershipInline,)


@admin.register(WorkGroup)
class WorkGroupAdmin(admin.ModelAdmin):
    inlines = (MembershipInline,)
