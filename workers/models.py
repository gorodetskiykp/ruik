from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, verbose_name='Пользователь')
    phone = models.CharField('Телефон', max_length=30, blank=True)

    def __str__(self):
        return self.user.username

    class Meta:
        ordering = ['user']
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class WorkGroup(models.Model):
    work_group_name = models.CharField('Название рабочей группы', max_length=100, blank=False, unique=True)
    messages_group = models.ForeignKey('grps.Group', verbose_name='Группа обслуживания')
    members = models.ManyToManyField(Profile, through='Membership')

    def __str__(self):
        return self.work_group_name

    class Meta:
        ordering = ['work_group_name']
        verbose_name = 'Рабочая группа'
        verbose_name_plural = 'Рабочие группы'

class Membership(models.Model):
    person = models.ForeignKey(Profile, related_name='memeber_person')
    group = models.ForeignKey(WorkGroup, related_name='memeber_work_group')
    is_head = models.BooleanField('Руководитель', default=True)
