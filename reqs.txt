Django==1.10.2
django-bootstrap3==7.1.0
django-formtools==1.0
django-mptt==0.8.6
django-multiupload==0.5.2
django-tables2==1.2.6
Pillow==3.4.2
