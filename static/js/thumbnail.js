function equalHeight(group) {
    var tallest = 0;
    group.each(function() {
        var thisHeight = $(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.each(function() { $(this).height(tallest); });
}

$(document).ready(function() {
    equalHeight($('.thumbnail'));
});

$('#click_hide').click(function(){
$('.nonactive_group').hide();
});

$('#click_show').click(function(){
$('.nonactive_group').show();
});

$('.check_thumbs').click(function(){
// $(":checkbox").change(function(){
  if (this.checked) {
    $('.nonactive_group').show();
  } else {
    $('.nonactive_group').hide();
  }
});

$('#to_work_button').on('click', function(event){
  event.preventDefault();
  var element = $(this);
  $.ajax({
    url : '/to_work_status/',
    type : 'GET',
    data : { message_id : element.attr("data-id")},
  });
});
