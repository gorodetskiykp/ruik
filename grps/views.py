from django.views.generic import ListView, DetailView
from django.contrib import messages

# from django_tables2 import SingleTableMixin

# from .tables import MessageTable
from .models import Group

class IndexView(ListView):
    context_object_name = 'latest_groups_list'

    def get_queryset(self):
        return Group.objects.all()

class DetailView(DetailView):
    model = Group
    context_object_name = 'group'
