from django.db import models
from datetime import datetime
# from django.contrib.auth.models import User


class GroupType(models.Model):
    group_type = models.CharField(max_length=250, verbose_name='Тип объекта')

    def __str__(self):
        return self.group_type

    class Meta:
        verbose_name = 'Тип объекта'
        verbose_name_plural = 'Типы объектов'

class Group(models.Model):
    group_name      = models.CharField('Название группы', max_length=250)
    date_create     = models.DateTimeField('Дата создания', auto_now_add=True)
    date_updated    = models.DateTimeField('Дата редактирования', auto_now=True)
    group_type      = models.ForeignKey(GroupType, null=True, blank=True, verbose_name='Тип группы', related_name='groups')
    category_set    = models.ForeignKey('mssgs.CategoriesSet', null=True, blank=True, verbose_name='Набор категорий обращений', related_name='groups')
    date_end        = models.DateField('Срок действия группы', null=True, blank=True)
    glyphicon_name  = models.CharField('Название иконки', max_length=50, default='glyphicon-check')
    glyphicon_color  = models.CharField('Цвет иконки', max_length=50, default='black')

    def __str__(self):
        return self.group_name

    class Meta:
        ordering = ['group_name']
        verbose_name = 'Объект'
        verbose_name_plural = 'Объекты'

# class WorkGroup(models.Model):
#     name = models.ManyToManyField(User)
