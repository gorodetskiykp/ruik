from django.contrib import admin

from .models import Group, GroupType

class GroupAdmin(admin.ModelAdmin):
    fields = ['group_name', 'group_type', 'category_set', 'date_end', 'glyphicon_name', 'glyphicon_color']
    list_display = ['group_name', 'date_create', 'date_updated', 'group_type', 'category_set']
    search_fields = ['group_name']

class GroupTypeAdmin(admin.ModelAdmin):
    pass

admin.site.register(Group, GroupAdmin)
admin.site.register(GroupType, GroupTypeAdmin)
