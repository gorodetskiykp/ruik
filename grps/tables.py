import django_tables2 as tables
from django_tables2.utils import A
from mssgs.models import Message

class MessageTable(tables.Table):
    message_text = tables.LinkColumn('mssgs:detail', text=lambda record: record.message_text, args=[A('pk')])
    class Meta:
        model = Message
        attrs = {'class': 'table table-bordered'}
        fields = ['id', 'message_text', 'category', 'date_create']

# from mssgs.tables import MessageTable
#
# class GroupMessageTable(MessageTable):
#     class Meta(MessageTable.Meta):
#         fields = ['message_text', 'date_create']
#         exclude = ['group']
